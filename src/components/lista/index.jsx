import {FlatList, Text, TouchableOpacity, View} from "react-native";

import React from "react";
import { styles } from "./styles";

const Lista = ({events, onHandlerEvent}) => {
  return (
    <View style={styles.listContainer}>
      <FlatList // Se utiliza para crear lista de componentes y que se pueda deslizar
        data = {events} // Fuente de informacion para crear modulos  
        renderItem = {({item}) => (
        <TouchableOpacity style={styles.itemlist} onPress={onHandlerEvent.bind(this, item.id)}> 
          <Text style={styles.item}>{item.value}</Text>
        </TouchableOpacity>
        )}  
        keyExtractor={(item) => item.id}
      />
    </View>
  )
}
export default Lista; 