import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
    item: {
        marginHorizontal: 20, // Margen horizontal con el componente
        color: "black", // Color del texto 
        fontSize: 16, // Tamaño de la fuente de texto
        fontWeight: "bold", // Letra en negrita
      },
      itemlist: {
        height: 80,
        justifyContent: "center",
        backgroundColor: "#ccb275",
        borderRadius: 15,
        marginVertical: 15, // Separación vertical de los modulos
      },
      listContainer: {
        flex: 1,
        backgroundColor: "#fff",
        marginVertical: 20,
      },
});
