import { Button, Modal, Text, View } from "react-native";

import React from "react";
import { styles } from "./styles";

const ModalFondo = ({visible, item, onDelete, onClose}) => {
    return (
        <Modal visible={visible} animationType="slide">
            <View style={styles.modalContainer}>
                <Text style={styles.modalTitle}>Detalle del evento</Text>
                <View style={styles.modalDetailContainer}>
                    <Text style={styles.modalDetailMessage}>¿Estas seguro de eliminar el evento?</Text>
                    <Text style={styles.selectedEvent}>{item?.value}</Text>
                </View>
                <View style={styles.buttonContainer}>
                    <Button title="Cancelar" color="#5252BC" onPress={onClose}></Button>
                    <Button title="Eliminar" color="#5252BC" onPress={onDelete}></Button>
                </View>
            </View>
        </Modal>
    )
}
export default ModalFondo;