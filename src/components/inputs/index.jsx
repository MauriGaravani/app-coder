import {Button, TextInput, View} from "react-native";

import React from "react";
import { styles } from "./styles"; // Importo la hoja de estilos creada para el componente

const Input = ({placeholder, value, onChangeText, buttonTitle, onHandlerButton}) => { // Defino los parametros de la funcion
    return (
            <View style={styles.inputContainer}>
                <TextInput 
                placeholder={placeholder} // Muestra en una orden en el fondo del input text
                style={styles.input} 
                value={value} // value es el valor de la entrada de texto
                onChangeText={onChangeText} // Devuelve el texto modificado 
                />
                <Button title={buttonTitle} onPress={onHandlerButton}/>
            </View>
            )
}
export default Input;