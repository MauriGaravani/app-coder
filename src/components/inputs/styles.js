import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  inputContainer: {
    width: "100%",
    flexDirection: "row",
    marginTop: 60,
    alignItems: "center", // Aliena los item en el medio
    //justifyContent: "space-arround", // Ajusta los componentes dandole un espaciado NO COMPILA EN ANDRIOD
  },
  input: {
    width: "80%",
    fontSize: 20,
    borderBottomWidth: 1, // Linea debajo de la entrada de texto
    borderBottomColor:"#5252BC", // Color de la linea debajo de la entrada de texto
    color: "#212121", // Color de la letra del input text 
  },
});

