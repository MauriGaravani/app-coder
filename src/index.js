import { Input, Lista, ModalFondo } from "./components"; // Importo los modulos creados desde ./components

import {View} from "react-native"; // Importo los modulos a utilizar desde react-native
import {styles} from "./styles"; // Importo la hoja de estilos creada de manera externa 
import {useState} from "react"; // Importo el hook useState

export default function App() {
  
  const [text, setText] = useState(""); // Declaración de una variable de estado que llamaremos "text"
  const [events, setEvents] = useState([]); // Declaración de una variable de estado que llamaremos "events"
  const [modalVisible, setModalVisible] = useState(false); // Declaración de una variable de estado que llamaremos "modalVisible"
  const [selectedEvent, setSelectedEvent] = useState(null); // Declaración de una variable de estado que llamaremos "selectedEvent"
  
  function onAddEvent () 
  {
    if (text.length === 0) return;
    setEvents([
      ...events, // Se usan los ... para guardar los elementos anteriores junto a los nuevos
      {
        id: Math.random().toString(), // Asigno una id con un numero random y lo convierto en String
        value: text // Guardo el texto del input en la variable value
      }
    ]);
    setText(""); // Seteo el text en un string vacio para borrar lo escrito luego de enviar el mensaje
  }

  function onHandlerEvent (id) 
  {
    // console.log("Entre") // Muestra el dato en la terminal de la computadora
    // console.warn("Entre") //  Muestra el dato dentro de la pantalla del dispositivo a utilizar
    setModalVisible(!modalVisible);
    const selectedEvent = events.find(event => event.id === id);
    setSelectedEvent(selectedEvent); 
  }

  function CloseModal ()
  {
    setModalVisible(!modalVisible);
    setModalVisible(null);
  }
  
  function DeleteItem (id)
  {
    setEvents(events.filter(event => event.id !== id))
    setModalVisible(!modalVisible);
  }
   
  return (
    <View style={styles.container}>
      
      <Input
          buttonTitle = "Agregar"
          placeholder = "Ingresa un nuevo evento" // Muestra en una orden en el fondo del input text
          value = {text} // value es el valor de la entrada de texto
          onChangeText = {(text) => setText(text)} // Devuelve el texto modificado 
          onHandlerButton = {onAddEvent} // La funcion que se ejecuta al presionar el boton
          style = {styles.input} 
      />
      
      <Lista
        events = {events}
        onHandlerEvent = {onHandlerEvent}
      />
   
      <ModalFondo
        visible = {modalVisible}
        item = {selectedEvent}
        onClose = {CloseModal}
        onDelete = {DeleteItem.bind(this, selectedEvent?.id)}
      />
        
    </View>
  );
}